import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';

const initialForm = {
    name:'',
    id: null
}

const StateForm = ({createDataState, updateDataState, dataToEdit, setDataToEdit}) => {
    const history = useNavigate();

    const [form, setForm] = useState(initialForm)

    useEffect(() => {
        if(dataToEdit) {
            setForm(dataToEdit)
        } else {
            setForm(initialForm);
        }
    }, [dataToEdit])

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if(!form.name) {
            alert("Datos incompletos")
            return;
        }

        if (form.id === null) {
            createDataState(form);
        } else {
            updateDataState(form)
        }
        history('/state')

        handleReset();
    }

    const handleReset = (e) => {
        setForm(initialForm);
        setDataToEdit(null)
    }

  return (
    <div>
        <br />
        <Container>
        <h3>{dataToEdit ? "Editar Estado" : "Crear Estado"}</h3>
        <br />
            <Form onSubmit={handleSubmit}>
                <FormGroup>
                    <Label>
                    {dataToEdit ? "Introduzca los datos a editar y presione CONFIRMAR" : "Nombre del Estado"}
                    </Label>
                    <Input type='text' name='name' placeholder='Introduzca el nombre del estado' onChange={handleChange} value={form.name} />
                </FormGroup>
                <FormGroup>
                    <Button color='success' type='submit' value='Enviar'>{dataToEdit ? "Confirmar" : "Aceptar"}</Button>    
                    {' '}
                    <Link to='/State'>
                    <Button color='danger' type='reset' value='Limpiar' onClick={handleReset}>Cancelar</Button>
                    </Link>
                </FormGroup>
            </Form>
        </Container>
    </div>
  )
}

export default StateForm