import React, { useEffect, useState } from 'react'
import StateForm from './StateForm';
import StateTable from './StateTable';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { helpHttp } from '../../Helpers/helpHttp';
import Loader from '../../../api/Loader';
import Message from '../../../api/Message';
import swal from 'sweetalert';

const State = () => {
  const [db, setDb] = useState(null);

  const [dataToEdit, setDataToEdit] = useState(null);

  const [error, setError] = useState(null);

  const [loading, setLoading] = useState(false);

  let api = helpHttp();
  let url = "http://localhost:8000/api/states/";
  let urlEdit = "http://localhost:8000/api/states";

  useEffect(() => {
    setLoading(true);
    helpHttp().get(url).then((res) => {
      console.log(res)
      if(!res.err){
        setDb(res);
        setError(null);
      } else {
        setDb(null)
        setError(res);
      }

      setLoading(false);
    });
  }, [url])

  const createDataState = (data) => {
    data.id = Date.now();
      // console.log(data);

    let options = {
      body:data, 
      headers: {"content-type":"application/json"},
    };

    api.post(url, options).then((res) => {
      console.log(res);
      if(!res.err){
        setDb([...db, res]);
      } else {
        setError(res);
      }
    });        

  };

  const updateDataState = (data) => {
      let endpoint = `${urlEdit}/${data.id}`;
      // console.log(endpoint);
      let options = {
        body:data, 
        headers: {"content-type":"application/json"},
      };
      api.put(endpoint, options).then((res) => {
      // console.log(res);
      if(!res.err){
        let newData = db.map((el) => (el.id === data.id ? data : el));
        setDb(newData);
        swal({text: "El estado se ha editado con exito",
        icon: "success"})
      } else {
        setError(res);
      }
    });   
  };

  const deleteDataState = (id) => {
    swal({
        title: "Eliminar",
        text: "Estas seguro que deseas eliminar este estado?",
        icon: "error",
        buttons: ["No", "Si"]
    }).then(respuesta => {
        if(respuesta){
            let endpoint = `${urlEdit}/${id}`;
            let options = {
                headers: {"content-type":"application/json"},
            };
            api.del(endpoint, options).then(res => {
                if(!res.err){ 
                    let newData = db.filter((el) => el.id !== id);
                    setDb(newData);

                    swal({text: "El estado ha sido eliminado con exito",
                    icon: "success"})
                    
                } else {
                    setError(res);
                }
            }); 
        } else {
            return;
        }
    })
  };


  // const createDataState = (data) => {
  //   data.id = Date.now();
  //   setDb([...db, data])
  //   // console.log(data);
  // };

  // const updateDataState = (data) => {
  //   let newData = db.map((el) => el.id === data.id ? data : el);
  //   setDb(newData);
  // };

  // const deleteDataState = (id) => {
  //   let newData = db.filter((el) => el.id !== id);
  //   setDb(newData);
  // };
    

  return (
    <Router>
      <div>
        <Routes>
          <Route path= "/state" 
            element={
              <div>
                {loading && <Loader />}
                {error && (
                  <Message 
                    msg={`Error ${error.status}: ${error.statusText}`} 
                    bgColor="red" 
                  />
                )}

                {db && <StateTable 
                  data = {db}
                  setDataToEdit={setDataToEdit}
                  deleteDataState={deleteDataState}
                />}
              </div>
            } 
          />
          
          <Route path="/StateForm" 
            element={
              <StateForm 
                createDataState={createDataState}
                updateDataState={updateDataState}
                dataToEdit={dataToEdit}
                setDataToEdit={setDataToEdit}
              />
            }
          />
        </Routes>
      </div>
    </Router>
  )
}

export default State
