import React from 'react'
import { Link } from 'react-router-dom'
import StateTableRow from './StateTableRow'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Container, Table } from 'reactstrap'

const StateTable = ({data, setDataToEdit, deleteDataState}) => {
  return (
    <div>
        <br />
        <Container>
            <h3>Lista de Estados</h3>
            <br />
            <Link to='/StateForm'>
            <Button color='success'>Crear Estado</Button>
            </Link>
            <br />
            <br />
            <Table>
                <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {data.length > 0 ? (
                        data.map((el) => (
                            <StateTableRow 
                                key={el.id} 
                                el={el}
                                setDataToEdit={setDataToEdit}
                                deleteDataState={deleteDataState}
                            />     
                        ))
                    ):(
                        <tr>
                            <td colSpan='2'>Sin datos</td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Container>
    </div>
  )
}

export default StateTable