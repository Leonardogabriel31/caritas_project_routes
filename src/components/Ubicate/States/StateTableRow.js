import React from 'react'
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

const StateTableRow = ({el, setDataToEdit, deleteDataState}) => {
    let {id, name} = el;

  return (
    <tr>
        <td>{name}</td>
        <td>
            <Link to='/StateForm'>
                <Button color='primary' onClick={() => setDataToEdit(el)}>Editar</Button>
            </Link>
            {' '}
            <Button color='danger' onClick={() => deleteDataState(id)}>Eliminar</Button>
        </td>
    </tr>
  )
}

export default StateTableRow